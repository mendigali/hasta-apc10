require("dotenv").config();

const TelegramBot = require("node-telegram-bot-api");

const telegramBotToken = process.env.TELEGRAM_BOT_TOKEN;

if (!telegramBotToken) {
  throw new Error("TELEGRAM_BOT_TOKEN is not defined");
}

const bot = new TelegramBot(telegramBotToken, { polling: true });

bot.on("message", (msg) => {
  const chatId = msg.chat.id;
  const message = msg.text;

  bot.sendMessage(chatId, `Your message: ${message}`);
});
